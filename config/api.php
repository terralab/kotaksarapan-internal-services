<?php

return [
    'email' => env('API_PUBLIC_EMAIL', 'public.ks@terralab.co'),
];
