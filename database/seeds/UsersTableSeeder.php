<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'public.user',
        	'email' => 'public.ks@terralab.co',
            'phone' => '021',
        	'password' => Hash::make('kotaksarapan20!&'),
        ]);    
    }
}
