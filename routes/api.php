<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('forgot', 'Api\V1\AuthenticateController@forgotPassword');
Route::post('public', 'Api\V1\AuthenticateController@publicLogin');
Route::get('history', 'Api\V1\PaymentController@histroyPayment');
Route::post('address', 'Api\V1\AddressController@checkaddress');
Route::get('verify/{email}/{verifyToken}', 'Api\V1\AuthenticateController@sendEmailDone')->name('sendEmailDone');

Route::group(['namespace' => 'Api\V1', 'middleware' => 'auth:api'], function () {
    Route::resource('meals', 'MealController', ['only' => ['create', 'store', 'update', 'destroy']]);
    Route::resource('categories', 'CategoryController', ['only' => ['create', 'store', 'update', 'destroy']]);
});

Route::group(['namespace' => 'Api\V1', 'middleware' => 'jwt.auth'], function () {
    // meal route relations
    Route::get('meals/favorite', 'MealController@favorite');
    Route::get('meals/search', 'MealController@search');
    Route::get('meals/categories/{category}', 'MealController@category');
    Route::get('meals/cook/{cook}', 'MealController@cook');

    // cook route relations
    Route::get('cooks/{id}/meals', 'CookController@mealsCookById');
    Route::resource('cooks', 'CookController', ['only' => ['create', 'store', 'update', 'destroy']]);

    Route::post('order', 'OrderController@store');
    Route::post('listorder', 'OrderitemController@store');

    //native routes
    Route::resource('meals', 'MealController', ['only' => ['index', 'show']]);
    Route::resource('users', 'UserController', ['only' => ['index', 'show']]);
    Route::resource('cooks', 'CookController', ['only' => ['index', 'show']]);
    Route::resource('categories', 'CategoryController', ['only' => ['index', 'show']]);
    Route::resource('addresses', 'AddressController', ['only' => ['index', 'show']]);
    Route::resource('cooks', 'CookController', ['only' => ['show']]);

    Route::post('login', 'AuthenticateController@login');
    Route::post('logout', 'AuthenticateController@logout');
    Route::post('register', ['uses' => 'AuthenticateController@register']);
    Route::get('whois', 'AuthenticateController@getAuthenticatedUser');
    Route::post('loginprovider', 'AuthenticateController@loginProvider');
    Route::post('update', 'UserController@update');
    Route::post('transaction', 'TransactionController@store');
    Route::post('payment', 'TransactionController@update');
    Route::post('topup', 'TransactionController@topup');
});
