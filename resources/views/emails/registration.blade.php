@component('mail::message')
#Dear {{ $user->email }},

<br>Your Registration Success<br>

 To verify email <a href="{{route('sendEmailDone',["email" => $user->email,"verifyToken"=>$user->verifyToken])}}">click here</a>


Regards,<br>
{{ config('app.name') }}
@endcomponent
