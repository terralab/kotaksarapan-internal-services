@component('mail::message')
#Dear {{ $user->email }},

<br>It appears that you would like to reset your password. Please click on the link below to setup a new password:<br>
@component('mail::button', ['url' => 'http://kotaksarapan.com/changepassword.php?{{ ($user->password) }}'])
Reset Here
@endcomponent

<br>If you did not request a password reset, please ignore this email. Your current password will continue to work.<br>

Regards,<br>
{{ config('app.name') }}
@endcomponent
