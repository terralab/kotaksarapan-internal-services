<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">
<!-- disable auto telephone linking in iOS -->
<title></title>
<style type="text/css">
  /* ========== Custom Font Import ========== */
            @import url(http://daks2k3a4ib2z.cloudfront.net/0globals/avenirnextpro-webfont.css);

            /* RESET STYLES */
            body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica, Arial, "Lucida Grande", sans-serif;}
            table{/*border-collapse:collapse;*/ border-spacing: 0px; }
            table[id=bodyTable] {width:100%!important;margin:auto;max-width:600px!important;color:#7A7A7A;font-weight:normal;}
            img, a img{border:0; border-style:none;border-color: #ffffff; outline:none; text-decoration:none;height:auto; line-height:100%;}
            a {text-decoration:none !important; font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica, Arial, "Lucida Grande", sans-serif;}
            h1, h2, h3, h4, h5, h6{color:#2095f2; font-weight:500; font-family:'AvenirNextLTPro-Medium','Avenir Next','HelveticaNeueMedium','HelveticaNeue-Medium','Helvetica Neue Medium','HelveticaNeue','Helvetica Neue',Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:17px;margin-right:0;margin-bottom:13px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}

            /* CLIENT-SPECIFIC STYLES */
            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
            #outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
            img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
            body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; /*font-weight:normal!important;*/} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
            .ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 13px !important;} /* Force hotmail to push 2-grid sub headers down */

            /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

            /* ========== Page Styles ========== */
            h1{display:block;font-size:26px;font-style:normal;font-weight:500;line-height:100%;}
            h2{display:block;font-size:20px;font-style:normal;font-weight:500;line-height:120%;}
            h3{display:block;font-size:17px;font-style:normal;font-weight:500;line-height:110%;}
            h4{display:block;font-size:18px;font-style:italic;font-weight:500;line-height:100%;}
            .flexibleImage{height:auto;}
            .linkRemoveBorder{border-bottom:0 !important;}
            table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}

            body, #bodyTable{background-color:#F0F1F3;}
            #emailHeader{background-color:#F0F1F3;}
            #emailBody{background-color:#FFFFFF; /*border-collapse:collapse;*/ border-spacing: 0px; border:1px solid #E0E1E2; box-shadow: 0 0 15px #E8EAEC;-moz-box-shadow: 0 0 15px #E8EAEC;-webkit-box-shadow: 0 0 15px #E8EAEC;border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px}
            #emailFooter{background-color:#F0F1F3;}
            .textContent, .textContentLast{color:#6B7075; font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica; font-size:16px; line-height:125%; text-align:left;}
            .textContent a, .textContentLast a{color:#2095f2; text-decoration:none;}
            .nestedContainer{background-color:#F6F7F8; border:1px solid #F3F3F5;}
            .emailButton{background-color:#205478; border-collapse:separate;}
            .buttonContent{color:#FFFFFF; font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
            .buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
            .emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
            .emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
            .emailCalendarDay{color:#205478; font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
            .imageContentText {margin-top: 10px;line-height:0;}
            .imageContentText a {line-height:0;}
            #invisibleIntroduction {display:none !important; font-size:1px} /* Removing the introduction text from the view */

            .ios-footer { color: #8D9196; text-decoration: none !important}
            .ios-footer a { color: #8D9196; text-decoration: none !important; }

            /*FRAMEWORK HACKS & OVERRIDES */
            span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
            span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
            span[class=ios-color-hack3] a {color:#6B7075!important;text-decoration:none!important;}

            .a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}


            /* MOBILE STYLES */
            @media only screen and (max-width: 615px){
                /*////// CLIENT-SPECIFIC STYLES //////*/
                body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */


                table[id="emailHeader"], table[id="emailBody"], table[id="emailFooter"] {width:95% !important;}

                table[class="flexibleContainer"] {width:100% !important;}


                td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}

                td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important;}
                img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
                img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}



                table[class="flexibleContainerBoxNext"]{padding-top: 30px !important;}

                table[class="emailButton"]{width:100% !important;}
                td[class="buttonContent"]{padding:0 !important;}
                td[class="buttonContent"] a{padding:15px !important;}


                /* FULL-WIDTH TABLES */
                table[class="responsive-table"]{
                  width:100%!important;
                }

                /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
                td[class="padding"]{
                  padding: 10px 0px 10px 0px !important;
                  text-align: center;
                }

                /* ADJUST BUTTONS ON MOBILE */
                td[class="mobile-wrapper"]{
                    padding: 10px 5% 15px 5% !important;
                }

                table[class="mobile-button-container"]{
                    margin:0 auto;
                    width:100% !important;
                }

                a[class="mobile-button"]{
                    width:80% !important;
                    padding: 15px !important;
                    border: 0 !important;
                    font-size: 16px !important;
                }

            }


            @media only screen and (-webkit-device-pixel-ratio:.75){
            /* Put CSS for low density (ldpi) Android layouts in here */
            }

            @media only screen and (-webkit-device-pixel-ratio:1){
            /* Put CSS for medium density (mdpi) Android layouts in here */
            }

            @media only screen and (-webkit-device-pixel-ratio:1.5){
            /* Put CSS for high density (hdpi) Android layouts in here */
            }
            /* end Android targeting */

            /* CONDITIONS FOR IOS DEVICES ONLY
            =====================================================*/
            @media only screen and (min-device-width : 320px) and (max-device-width:568px) {

            }
            /* end IOS targeting */
</style>
<center style="background-color:#F0F1F3;">
  <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
    <tbody>
      <tr>
        <td align="center" valign="top" id="bodyCell">
          <!-- // EMAIL HEADER -->
          <!-- PREHEADER TEXT -->
          <table bgcolor="#F0F1F3" border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">
            <!-- HEADER ROW // -->
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td valign="top" width="600" class="flexibleContainerCell">
                                  <!-- CONTENT TABLE // -->
                                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important;">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                            <tbody>
                                              <tr>
                                                <td align="center" class="textContent">
                                                  <div style="font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif; font-size:1px;color:#F0F1F3;text-align:center;line-height:120%; display: block"></div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // END -->
            </tbody>
          </table>
          <!-- // END -->
          <!-- EMAIL HEADER W LOGO // -->
          <table bgcolor="#F0F1F3" border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">
            <!-- HEADER ROW // -->
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td valign="top" width="600" class="flexibleContainerCell">
                                  <!-- CONTENT TABLE // -->
                                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="middle" style="display:block; text-align:center;">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                            <tbody>
                                              <tr>
                                                <td align="center" class="">
                                                    <h1 style="color:#2095f2;line-height:125%;font-family:'ProximaNova-SemiBold';font-size:50px;font-weight:600;margin-bottom:15px;text-align:center;text-shadow: 0 0 3px #fff, 0 0 5px #4ccfed;">
                                                    <center>
                                                    	<img src="http://beta.kotaksarapan.com/statics/images/ks-logo.png" class="flexibleImage" alt="#" title="#"
                                              border="0"></center>
                                                    </h1>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // END -->
            </tbody>
          </table>
          <!-- // END -->
          <!-- START WHITE CONTAINER SECTION -->
          <!-- NOTE: REMOVE margin-bottom: 20px; FROM STYLES IF THERE's ONLY ONE CONTAINER
            -->
          <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody" style="margin-bottom: 5px; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px">
            <!-- MODULE ROW // -->
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF; border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0;" bgcolor="#3F8ECC">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <!-- // CONTENT TABLE -->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->
              <!-- MODULE ROW // -->
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="imageContent">
                                          <a href="#" target="_blank" style="color:#6B7075;">
                                            
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- // CONTENT TABLE -->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->
              <!-- MODULE ROW // -->
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="top" style="padding-top: 40px;padding-bottom:5px;">
                                          <!-- CONTENT TABLE // -->
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="textContent">
                                                  <div style="text-align:justify;font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;margin-bottom:0;margin-top:3px;color:#6B7075;line-height:135%;">
                                                    <br>Dear {{ $user->email }},<br>
<br>It appears that you would like to reset your password. Please click on the link below to setup a new password:<br>
<br>http://kotaksarapan.com/changepassword.php?{{ ($user->password) }}
<br>Please copy the link to your browser if you are unable to click in email.<br>
<br>If you did not request a password reset, please ignore this email. Your current password will continue to work.<br>
<br>Regards,
<br>Kotak Sarapan team.
                                                    <br /><br />
                                                    <!-- <p style="text-align: center;">Download KotakSarapan di <a href="https://play.google.com/store/apps/details?id=com.kotaksarapan">Android</a> atau <a href="https://itunes.apple.com/id/app/kotak-sarapan/id1012268058?mt=8">iOS</a> Sekarang Juga!</p> -->
                                                  </div>
                                                  <div style="text-align:center;font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;margin-bottom:0;margin-top:3px;color:#6B7075;line-height:135%;"></div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <br>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="top" style="padding-bottom:18px;padding-top:10px;">
                                          <!-- CONTENT TABLE // -->
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="textContent">
                                                  <div style="text-align:right;font-family:'AvenirNextLTPro-Regular','Avenir Next','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;margin-bottom:0;margin-top:3px;color:#6B7075;line-height:125%;">
                                                    <!-- WEBFLOW LOGO // -->
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                      <tbody>
                                                        <tr>
                                                          <td valign="top" align="left" class="textContent" style="text-align:center;">
                                                            <p style="color: #2095f2;text-align: right;font-weight: bold;letter-spacing: 1.5px;"></p>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!-- // LOGO -->
                                                  </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <!-- // CONTENT TABLE -->
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr style="padding-top:0;">
                        <td align="center" valign="top">
                          <table border="0" cellpadding="30" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td style="padding-top:0;" align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                    <tbody>
                                      <tr>
                                        <td align="center" style="padding-bottom:0; padding-top:0;" class="padding">
                                          <table border="0" cellspacing="0" cellpadding="0" class="responsive-table" style="border: 1px solid #2095f2;">
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- // BULLETPROOF BUTTON -->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW W/ BUTTON -->
              <!-- MODULE ROW // -->
            </tbody>
          </table>
          <!-- END WHITE CONTAINER SECTION -->
          <!-- EMAIL FOOTER // -->
          <!-- The table "emailBody" is the email's container. Its width can be set to 100%
            for a color band that spans the width of the page. -->
          <table bgcolor="#F0F1F3" border="0" cellpadding="0" cellspacing="0" width="600" id="emailFooter">
            <!-- FOOTER ROW // -->
            <!-- To move or duplicate any of the design patterns in this email, simply move
              or copy the entire MODULE ROW section for each content block. -->
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                            <tbody>
                              <tr>
                                <td align="center" valign="top" width="600" class="flexibleContainerCell">
                                  <table border="0" cellpadding="25" cellspacing="0" width="100%">
                                    
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
            </tbody>
          </table>
          <!-- // END -->
        </td>
      </tr>
    </tbody>
  </table>
</center>
</html>
